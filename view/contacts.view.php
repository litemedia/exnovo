			<section class="no_scroll">
				<!-- dates section -->
				<div class="contacts">
					<h3>contact us</h3>

					<?php if ( !empty($status)) : ?>
					<h4><?= $status; ?></h4>
					<?php endif; ?>
					<!-- contact form -->
					<form action="" method="post" role="form">
						<div class="form-group">
							<label for="name">your name</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" required>
						</div>
						<div class="form-group">
							<label for="email">your e-mail</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Enter your e-mail" required>
						</div>
						<div class="form-group">
							<label for="message">your message</label>
							<textarea class="form-control" rows="6" id="message" name="message" placeholder="Enter your message" required></textarea>
						</div>
						<button type="submit" class="btn btn-default">Submit</button>
					</form>

				</div>
				<div class="band band--lower"></div>
			</section>