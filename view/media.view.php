			<!-- media section -->
			<div class="media-content">
				<!-- nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="active"><a href="#pics" role="tab" data-toggle="tab">pics</a></li>
					<!-- <li><a href="#video" role="tab" data-toggle="tab">video</a></li> -->
					<li><a href="#music" role="tab" data-toggle="tab">music</a></li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane fade in active media-tab" id="pics">
						<div class="media-tab-content">
							<!-- <div class="media-tab-content-item">
								<img src="images/image-960px-1.jpg" alt="Ex Novo Project">
							</div> -->
							<div class="media-tab-content-item">
								<a href="images/image-960px-2.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-2.jpg" alt="Ex Novo Project">
								</a>
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-3.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-3.jpg" alt="Ex Novo Project">
								</a>
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-4.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-4.jpg" alt="Ex Novo Project">
								</a>
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-5.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-5.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-6.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-6.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-7.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-7.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<!-- <div class="media-tab-content-item">
								<img src="images/image-960px-8.jpg" alt="Ex Novo Project">
							</div> -->
							<div class="media-tab-content-item">
								<a href="images/image-960px-9.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-9.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-10.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-10.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-11.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-11.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-12.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-12.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-13.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-13.jpg" alt="Ex Novo Project">
								</a>	
							</div>
							<div class="media-tab-content-item">
								<a href="images/image-960px-14.jpg" title="Ex Novo Images" data-gallery>
									<img src="images/image-960px-14.jpg" alt="Ex Novo Project">
								</a>	
							</div>
						</div>
					</div>
					<!-- <div class="tab-pane fade media-tab" id="video">VIDEO YOUTUBE</div> -->
					<div class="tab-pane fade media-tab" id="music">
						<div class="media-tab-content">
							<div class="media-tab-content-item">
								<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/164895649&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>	
							</div>
							<div class="media-tab-content-item">
								<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/171942221&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
			<div id="blueimp-gallery" class="blueimp-gallery">
			    <!-- The container for the modal slides -->
			    <div class="slides"></div>
			    <!-- Controls for the borderless lightbox -->
			    <h3 class="title"></h3>
			    <a class="prev">‹</a>
			    <a class="next">›</a>
			    <a class="close">×</a>
			    <a class="play-pause"></a>
			    <ol class="indicator"></ol>
			    <!-- The modal dialog, which will be used to wrap the lightbox content -->
			    <div class="modal fade">
			        <div class="modal-dialog">
			            <div class="modal-content">
			                <div class="modal-header">
			                    <button type="button" class="close" aria-hidden="true">&times;</button>
			                    <h4 class="modal-title"></h4>
			                </div>
			                <div class="modal-body next"></div>
			                <div class="modal-footer">
			                    <button type="button" class="btn btn-default pull-left prev">
			                        <i class="glyphicon glyphicon-chevron-left"></i>
			                        Previous
			                    </button>
			                    <button type="button" class="btn btn-primary next">
			                        Next
			                        <i class="glyphicon glyphicon-chevron-right"></i>
			                    </button>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>		
