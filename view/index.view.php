<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<html class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0" />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">

<title>Ex Novo Project - <?= $title ?></title>

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="js/frameworks/modernizr.min.js"></script>
<script src="js/frameworks/css3-mediaqueries.js"></script>
<script src="js/picturefill.min.js"></script>

</head>

<body>
	
	<div class="wrapper">
		<?php if ( $id !== '4' ) : ?>
			<img class="wrapper--background <?php echo $id== '5' ? 'opacity-down' : ''; ?>"  src="images/<?= $bg_image; ?>-IPAD.jpg" 
				srcset="images/<?= $bg_image; ?>-FULL.jpg 2048w, images/<?= $bg_image; ?>-LARGE.jpg 1024w, images/<?= $bg_image; ?>-MEDIUM.jpg 640w, images/<?= $bg_image; ?>-SMALL.jpg 320w"
				sizes="100%"
				alt="Ex Novo Project">
		<?php endif; ?>
		<div class="content">
			<div class="socialthings <?= $logo_positioning; ?>">
				<div class="logo-<?= $logo_color; ?>"></div>
				<ul class="socials">
					<li><a href="https://www.facebook.com/pages/Ex-Novo-Project/698504040228037"><div class="facebook"></div></a></li>
					<li><a href="https://www.youtube.com/channel/UC_uC9kyss1plhXYJwd44RPg"><div class="youtube"></div></a></li>
					<li><a href="http://instagram.com/exnovoproject"><div class="instagram"></div></a></li>
					<li><a href="https://soundcloud.com/ex-novo-project"><div class="soundcloud"></div></a></li>
				</ul>
			</div>
			<div class="<?php echo $id != '4' ? 'band ' : ''; ?>band--upper"></div>
			<!-- Regular Navigation Menu -->
			<nav class="main__nav">
				<ul>
					<li><a href="?p=1">the project<span class="hover-violet <?php echo $id == '1' ? 'active' : ''; ?>"></span></a></li>
					<li><a href="?p=2">music<span class="hover-blue <?php echo $id == '2' ? 'active' : ''; ?>"></span></a></li>
					<li><a href="?p=3">dates<span class="hover-orange <?php echo $id == '3' ? 'active' : ''; ?>"></span></a></li>
					<li><a href="?p=4">media<span class="hover-green <?php echo $id == '4' ? 'active' : ''; ?>"></span></a></li>
					<li><a href="?p=5">contacts<span class="hover-red <?php echo $id == '5' ? 'active' : ''; ?>"></span></a></li>	
				</ul>
			</nav>
			<!-- Mobile Navigation Menu -->
			<nav class="mobile__nav">
				<div class="mobile__nav--toggler text-center" id="toggler"><i class="fa fa-bars fa-lg"></i></div>
				<div class="mobile__nav--slider" id="slider">
					<ul>
						<li><a href="?p=1" class="btn-hover-violet">the project</a></li>
						<li><a href="?p=2" class="btn-hover-blue">music</a></li>
						<li><a href="?p=3" class="btn-hover-orange">dates</a></li>
						<li><a href="?p=4" class="btn-hover-green">media</a></li>
						<li><a href="?p=5" class="btn-hover-red">contacts</a></li>			
					</ul>	
				</div>
			</nav>
				
			<!-- include della pagina del contenuto	 -->
			<?php include 'controller/' . $content; ?>
			<!-- end include -->
		</div>
	</div>



<!-- SCRIPTS JS -->
    <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- PlaceHolder fallback for old browser -->
    <script src="js/frameworks/jquery.placeholder.js"></script>

	<!-- Bootstrap -->
	<!-- If you're using Bootstrap, uncomment the line below -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/frameworks/bootstrapValidator.min.js"></script>
	<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<script src="js/bootstrap-image-gallery.min.js"></script>

	<!-- Main -->
	<script src="js/scripts.js"></script> 
<!-- END SCRIPTS JS -->

</body>
</html>
