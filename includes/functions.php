<?php namespace Exnovo\Functions;

// Recupera la configurazione della pagina
function get_page_config($page_id, $conn) {
	$stmt = $conn->prepare('select * from exnovo_pages where id = :page_id');
	$stmt->bindParam('page_id', $page_id);
	$stmt->execute();

	return $stmt->fetch(\PDO::FETCH_ASSOC);
}

// connette al database
function connect($host, $username, $password, $database) {
	try {
		$conn = new \PDO("mysql:host=$host;dbname=$database",
						$username,
						$password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		return $conn;

	} catch (Exception $e) {
		return false;
	}
}

function send_contact_msg($post_data) {
	extract($post_data);

	$msg = <<<EOT
		<html>
		<h1>New Message !!</h1>
		<h4>From: $name</h3>
		<h4>E-mail: $email</h3>
		<p>$message</p>
		</html>
EOT;
	$intestazioni  = "MIME-Version: 1.0\r\n";
	$intestazioni .= "Content-type: text/html; charset=iso-8859-1\r\n";
	$intestazioni .= "From: Exnovo Mailer<mailer@exnovoproject.com>\r\n";
	$intestazioni .= "Bcc: bertog974@gmail.com\r\n";

	return mail('exnovoproject@gmail.com', 'New Contact Message :)', $msg, $intestazioni);
}