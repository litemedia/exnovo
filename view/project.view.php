<div class="band band--lower">
				<article>
					<p>Dear visitor,</p>
					<p>we are really pleased to tell you that the <strong>EX NOVO PROJECT</strong> has finally arrived! We are so excited! We have worked hard, between the harp, the computer and the loop machine, to compose, arrange and create a crazy show around our first love....<strong>THE HARP!</strong></p>   
					<p>Even though we come from the classical world and still work in it, we are passionate about exploring the possibilities of combining our instrument with <strong>ELECTRONICS</strong>.</p>
					<p>In the following pages, you will find some recordings, but it would really mean a lot to us to see you at our events... so have a look at the concert page as well!</p>
					<p>Lots of love, <strong>Vanja and Sylvana!</strong></p>
				</article>
			</div>