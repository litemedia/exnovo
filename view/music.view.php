			<section class="scroll">
				<!-- dates section -->
				<div class="musics">
					<h3>I. breton girls by the sea</h3>
					<h4>from "Two Postcards from Paul Gaugin"<br>Eleanor Turner</h4>
					<p>for prepared harp and loop machine</p>
				</div>
				<div class="musics">
					<h3>II. Parau Parau (Whispered Words)</h3>
					<h4>from "Two Postcards from Paul Gaugin"<br>Eleanor Turner</h4>
					<p>for harp and loop machine</p>
				</div>
				<div class="musics">
					<h3>Ex-In</h3>
					<h4>Sylvana Labeyrie</h4>
					<p>for mixed breathing and improvised harp</p>
				</div>
				<div class="musics">
					<h3>Twin Peaks</h3>
					<h4>Angelo Badalamenti</h4>
					<p>arr. for harp and loop machine by Vanja Contu</p>
				</div>
				<div class="musics">
					<h3>Nagoya Marimba</h3>
					<h4>Steive Reich</h4>
					<p>arr. for recorded harp and live harp by Sylvana Labeyrie</p>
				</div>
				<div class="musics">
					<h3>Alice in Escher's Wonderland</h3>
					<h4>Eleanor Turner</h4>
					<p>for harp and loop machine</p>
				</div>
				<div class="musics">
					<h3>Neon</h3>
					<h4>Tansy Davis</h4>
					<p>mix by Sylvana Labeyrie with improvised harp</p>
				</div>
				<div class="musics">
					<h3>Extravaganza</h3>
					<h4>Sylvana Labeyrie</h4>
					<p>for backing track and improvised harp</p>
				</div>
				<div class="musics">
					<h3>Moonchild</h3>
					<h4>King Crimson</h4>
					<p>arr. for backing track and live harp by Vanja Contu and Mauro Capovilla</p>
				</div>
				<div class="musics">
					<h3>Lullaby for Joanna</h3>
					<h4>Sylvana Labeyrie</h4>
					<p>for harp and loop machine</p>
				</div>
				<div class="band band--lower"></div>
			</section>
			
