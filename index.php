<?php namespace Exnovo\Functions;

require 'includes/config.php';
require 'includes/functions.php';

// connette il database
$conn = connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_INUSE);

if ( $conn ) {

	$page_id = isset($_GET['p']) ? (int)$_GET['p'] : 1;

	$page_config = get_page_config($page_id, $conn);

	if ( $page_config ) {	
		extract($page_config);
		require 'view/index.view.php';		
	} else {
		header('Location: index.php');
	}

} else die('Could not connect to the database :(');


